using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using System.Collections.Generic;


public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public TextMeshProUGUI textoCantidadRecolectados;
    public TextMeshProUGUI textoGanaste;
    public TextMeshProUGUI textoTimer;
    public float timer = 60;
    private int cont;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public SphereCollider col;
    private int SaltosRealizados;
    private bool EnSuelo;
    private int MaximosSaltos = 1;
    
    

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cont = 0;
        textoGanaste.text = "";
        setearTextos();
        SaltosRealizados = 0;
    }

    void Update()
    {
        EnSuelo = EstaEnPiso();

        if (EnSuelo)
        {
            SaltosRealizados = 0;
        }

        if (Input.GetKeyDown(KeyCode.R) || transform.position.y < 0)
        {
            transform.position = new Vector3(1, 6, 2);
        }

        if (Input.GetKeyDown(KeyCode.Space) && (EnSuelo || SaltosRealizados < MaximosSaltos))
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            SaltosRealizados++;
        }

        if (timer >= 0)
        {
            timer -= Time.deltaTime;
            textoTimer.text = "" + timer.ToString("f0");
        }

        
        if (timer <= 0)
        {
            textoGanaste.text = "Game Over";
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        if (textoGanaste.text == "WINNER")
        {
            Time.timeScale = 0;
        }
    }


    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Breads: " + cont.ToString();
        if (cont >= 10)
        {
            textoGanaste.text = "WINNER";
        }

    }


    void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

    }


}