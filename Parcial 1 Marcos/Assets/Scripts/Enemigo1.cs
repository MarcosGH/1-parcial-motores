using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;
    public ControlJugador Jugador1;
    public int Identificador;

    void Start()
    {
        hp = 100;
        jugador = GameObject.Find("Jugador");
    }

    
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador") == true && Identificador == 0)
        {
           
            Jugador1.transform.position = new Vector3(1, 6, 2);
        }
        
    }

}
