using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    public float masVelocidad = 1.0f;
    public float menosTamaño = 0.75f;
    
    private void aumentarJugador(Rigidbody JugadorRB)
    {
        JugadorRB.velocity *= masVelocidad;
        Vector3 nuevoTamaño = JugadorRB.transform.localScale * menosTamaño;
        JugadorRB.transform.localScale = nuevoTamaño;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            aumentarJugador(other.GetComponent<Rigidbody>());
            gameObject.SetActive(false);
            
        }
    }
}
