using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo5 : MonoBehaviour
{
    public Transform centroRotacion;
    public float velocidadRotacion = 80f;

    void Update()
    {
        transform.RotateAround(centroRotacion.position, Vector3.up, velocidadRotacion * Time.deltaTime);
    }
}
