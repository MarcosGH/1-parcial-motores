using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo4 : MonoBehaviour
{
    public Transform Jugador;
    public float fuerzaSalto = 8f;

    void Update()
    {
        if (Vector3.Distance(transform.position, Jugador.position) < 5f)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        }
    }
}
