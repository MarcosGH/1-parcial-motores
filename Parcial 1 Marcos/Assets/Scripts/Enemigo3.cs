using UnityEngine;

public class Enemigo3 : MonoBehaviour
{
    public float velocidad = 5f; 
    public float distancia = 10f; 
    private Vector3 posicionInicial;
    private bool direccionPositiva = true;

    private void Start()
    {
        
        posicionInicial = transform.position;
    }

    private void Update()
    {
        
        if (direccionPositiva)
        {
            transform.Translate(Vector3.forward * velocidad * Time.deltaTime);
        }
        else
        {
            transform.Translate(-Vector3.forward * velocidad * Time.deltaTime);
        }

        
        if (Vector3.Distance(posicionInicial, transform.position) >= distancia)
        {
            direccionPositiva = !direccionPositiva;
        }
    }
}