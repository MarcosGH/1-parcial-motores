using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo6 : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;
    public ControlJugador Jugador1;

    void Start()
    {
        hp = 100;
        jugador = GameObject.Find("Jugador");
    }


    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            
            other.transform.localScale *= 1.2f;

            Destroy(gameObject);
        }
    }

}
